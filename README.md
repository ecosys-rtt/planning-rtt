# Planning RTT

| Date      | Thématique                                                   |
|-----------|:-------------------------------------------------------------|
| ~~13 Avril~~  | ~~Introduction à R et représentation graphiques sous `ggplot2`~~|
| ~~4 Mai~~     | ~~Atelier Débuggage~~                                            |
| ~~25 Mai~~    | ~~Manipuler les données avec `tidyverse`~~                     |
| 15 Juin   | Atelier Débuggage    
| 6 Juillet | Atelier Débuggage |
| Juillet-Août  | Pause estivale |
| mi Septembre | Introduction à R |
| fin Septembre | Versionner son code avec git et RStudio |
